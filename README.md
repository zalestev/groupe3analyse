# README #

### What is this repository for? ###
* Nom du projet: Analyse de communautés dans les réseaux d'informations
* Etat du projet: Version Final Project Groupe 3
* Description: Outils de détection de communauté d'auteurs et de mise en relations avec leurs centres d'intérêts.

### Configuration? ###

* Mise en place et utilisation: 
    Install Python3 (avec les librairies) pour executer les fonctions d'analyses.
    Install Kivy pour pouvoir exécuter le client
* Configuration
* Dependencies : pandas, numpy, Collection.counter, hdf5, ...

### Contenu du projet ###

* Code source constitué:
* Backend: Ensemble de fonction développé sous python pour nos differentes axes d'analyses.
* Front end: Interface graphique pour la présentation des résultats

### Contribution ###

* Ce projet est réalisé sous l’encadrement et la supervision de Madame BENTAYEB, Omar BOUSSAID